import React from 'react'
import NavBar from './Project Component/NavBar'
import Footer from './Project Component/Footer'
import { Outlet, Route, Routes } from 'react-router-dom'
import CreateAccount from './Project Component/CreateAccount'
import CreateLogin from './Project Component/CreateLogin'
import VerifyEmail from './Project Component/VerifyEmail'
import VerifyEmailPage from './Project Component/VerifyEmail'
import RegistrationSuccessPage from './Project Component/RegistrationSuccess'

const WowProject = () => {
  return (
    <div>

    <Routes>
        <Route path = "/" element = {<div><NavBar></NavBar><Outlet></Outlet></div>}>
            <Route index element = {<div>Home Page</div>}></Route>

            {/* <Route path ="hello" element = {<div><Outlet></Outlet></div>}>
                <Route index element = {<div>hello page</div>}></Route>
              <Route path = ":id" element = {<div>Hello and more</div>}></Route> */}
                
              <Route path = "create" element = {<CreateAccount></CreateAccount>}></Route>
              <Route path = "verify" element = {<VerifyEmail></VerifyEmail>}></Route>
              <Route path = "login" element = {<CreateLogin></CreateLogin>}></Route>
               
      
              {/* Add the route for the verify-email token */}
              <Route path="registration-success" element={<RegistrationSuccessPage></RegistrationSuccessPage>} />
        <Route
          path="verify-email" element={<VerifyEmailPage />}querystring
        />


                {/* <Route path = "update" element = {<div><Outlet></Outlet></div>}>
                <Route index element = {<div>Update product</div>}></Route>
                <Route path = ":id" element = {<div>Form to add product</div>}></Route>

                </Route> */}
            </Route>

        {/* </Route> */}
    </Routes>

    </div>
  )
}

export default WowProject