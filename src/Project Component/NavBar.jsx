import React from 'react'
import { NavLink} from 'react-router-dom'

const NavBar = () => {
  return (
    <div><nav style = {{backgroundColor: "red"}}>
    <NavLink to= "/create" style={{marginLeft: "20px", color: "white"}}>Register</NavLink>
    <NavLink to= "/login" style={{marginLeft: "20px",  color: "white"}}>Login</NavLink>
    </nav></div>
  )
}

export default NavBar