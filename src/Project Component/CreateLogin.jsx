import { Formik, Form } from 'formik';
import * as yup from 'yup';
import React from 'react';
import FormikInput from '../Formik/FormikInput';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

const CreateLogin = () => {
  const navigate = useNavigate();

  const initialValues = {
    email: '',
    password: '',
  };

  const onSubmit = async (values, { setSubmitting }) => {
    try {
      const response = await axios.post('http://localhost:8000/users/login', values);
      console.log(response.data);
      navigate('/admin');
    } catch (error) {
      console.log('Unable to submit:', error);
    } finally {
      setSubmitting(false);
    }
  };

  const validationSchema = yup.object({
    email: yup.string().required('Email is required. '),
    password: yup.string().required('Password is required. '),
  });

  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {formik => (
          <Form>
            <FormikInput
              name="email"
              label="Email: "
              type="email"
              required={true}
            />
            <FormikInput
              name="password"
              label="Password:"
              type="password"
              required={true}
            />
            <button type="submit" disabled={formik.isSubmitting}>
              Login
            </button>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default CreateLogin;
