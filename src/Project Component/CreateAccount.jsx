import { Formik, Form } from 'formik';
import * as yup from "yup"
import React from 'react'
import FormikInput from '../Formik/FormikInput';
import FormikSelect from '../Formik/FormikSelect';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';




//Things done in this page are: 
//1. Create Product form
//2. Hit api onSubmit


const CreateAccount = () => {
    const navigate = useNavigate();
    let initialValues = {
        fName: '',
        lName: '',
        dob: '',
        profileImage: '',
        password: '',
        email: '',
        role: 'customer',
     }
     
     let onSubmit = async (info) => {
        try {
          let result = await axios({
            url: `http://localhost:8000/users`,
            method: "post",
            data: info,
          });
    
          console.log("created successfully");
          navigate("/registration-success")

        } catch (error) {
          console.log("unable to create");
        }
      };
      
     
     let validationSchema = yup.object({
        fName:yup.string().required("Full Name is required. "),
        lName:yup.string().required("Full Name is required. "),
        dob:yup.string().required("DOB is required. "),
        profileImage:yup.string().required("Profile Image is required. "),
        password:yup.string().required("Password is required. "),
        email:yup.string().required("Email is required. "),
        role:yup.string(),

    
     })

     let roleOptions = [
      {
          label: "Select Role",
          value: "",
          disabled: true,
      },
      {
          label: "Admin",
          value: "admin",
      },
      {
          label: "Customer",
          value: "customer",
      },
      {
          label: "superAdmin",
          value: "superadmin",
      },
  ]


  return (
    <div>
         <Formik 
        initialValues= {initialValues}
        onSubmit = {onSubmit}
        validationSchema = {validationSchema}>
            {
                (formik)=> {

         //for formik we need
         //name
         //label
         //type
         //onChange
         //required
         //option (radio, select) (array of object)   

                    return (
                   <Form>
                    <FormikInput
                        name = "fName"
                        label = "First Name: "
                        type = "text"
                        required = {true}
                    ></FormikInput>

                    <FormikInput
                        name = "lName"
                        label = "Last Name: "
                        type = "text"
                        required = {true}
                    ></FormikInput>

                    <FormikInput
                        name = "dob"
                        label = "DOB: "
                        type = "date"
                        required = {true}
                    ></FormikInput>

                    <FormikInput
                            name = "profileImage"
                            label = "Profile Image"
                            type = "text"
                            required = {true}
                        ></FormikInput>

                            <FormikInput
                                name = "email"
                                label = "Email: "
                                type = "email"
                                required = {true}
                            ></FormikInput>

                    <FormikInput
                            name = "password"
                            label = "Password:"
                            type = "password"
                            required = {true}
                        ></FormikInput>




              <FormikSelect name = "role" 
                label = "Role: " 
                required = {true}
                
                options = {roleOptions}
                >
                </FormikSelect>

                  <button type = "submit">Create Account</button>

                   </Form>
    )  }
            }
        </Formik>

    </div>
  )
}

export default CreateAccount