import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

const VerifyEmailPage = () => {
  const navigate = useNavigate();
  const [verificationStatus, setVerificationStatus] = useState('');

  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search);
    const token = urlParams.get('token');

    if (token) {
      verifyEmail(token);
    } else {
      setVerificationStatus('Invalid verification token.');
    }
  }, []);

  const verifyEmail = async (token) => {
    try {
      const response = await axios.post('http://localhost:8000/users/verify-email', null, {
        params: {
          token: token,
        },
      });

      const { data } = response;
      if (data.success) {
        setVerificationStatus('Email verification successful!');
      } else {
        setVerificationStatus(`Email verification failed: ${data.message}`);
      }
    } catch (error) {
      console.error(error);
      setVerificationStatus('An error occurred during email verification. Error: ' + error.message);
    }
  };

  return (
    <div>
      <h1>Email Verification Page</h1>
      <p>{verificationStatus}</p>
      <button onClick={() => navigate('/login')}>Login</button>
    </div>
  );
};

export default VerifyEmailPage;
