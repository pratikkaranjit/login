import React from 'react';

const RegistrationSuccessPage = () => {
  return (
    <div>
      <h1>Registration Successful!</h1>
      <p>Please check your email for verification.</p>
    </div>
  );
};

export default RegistrationSuccessPage;
